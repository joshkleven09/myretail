# myRetail RESTful Service
###Case Study
myRetail is a rapidly growing company with HQ in Richmond, VA and over 200 stores across the east coast. myRetail wants to make its internal data available to any number of client devices, from myRetail.com to native mobile apps. 
The goal for this exercise is to create an end-to-end Proof-of-Concept for a products API, which will aggregate product data from multiple sources and return it as JSON to the caller. 
Your goal is to create a RESTful service that can retrieve product and price details by ID. The URL structure is up to you to define, but try to follow some sort of logical convention.

Build an application that performs the following actions: 

- Responds to an HTTP GET request at /products/{id} and delivers product data as JSON (where {id} will be a number). 

    - Example product IDs: 15117729, 16483589, 16696652, 16752456, 15643793

- Example response: {"id":13860428,"name":"The Big Lebowski (Blu-ray) (Widescreen)","current_price":{"value": 13.49,"currency_code":"USD"}}

- Performs an HTTP GET to retrieve the product name from an external API. (For this exercise the data will come from redsky.target.com, but let’s just pretend this is an internal resource hosted by myRetail) 

- Example: http://redsky.target.com/v2/pdp/tcin/13860428?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics

- Reads pricing information from a NoSQL data store and combines it with the product id and name from the HTTP request into a single response. 

- BONUS: Accepts an HTTP PUT request at the same path (/products/{id}), containing a JSON request body similar to the GET response, and updates the product’s price in the data store. 

###Dependencies
- Java 1.8

###Instructions to Run (Mac)
Ensure that Java JDK 8 is installed

1. Clone https://bitbucket.org/joshkleven09/myretail

2. Navigate to cloned folder in Terminal

3. Run the following command to build the application and run tests

   ```
   $ ./gradlew build
   ```
   
4. To run the application, make sure nothing is running on port 8080 and run:

   ```
   $ java -jar build/libs/myRetail-1.0.0.jar
   ```
   
###Instructions to Run (Windows)
Ensure that Java JDK 8 is installed and in the environment PATH variable

1. Clone https://bitbucket.org/joshkleven09/myretail

2. Navigate to cloned folder in Command Prompt

3. Run the following command to build the application and run tests:

   ```
   gradlew build
   ```
   
4. To run the application, make sure nothing is running on port 8080 and run:

   ```
   java -jar build/libs/myRetail-1.0.0.jar
   ```
   
###API Format
**GET: /api/v1/products/{id}**

- Returns the request product IDs name and pricing information.

- Response codes:

    - 200: Product successfully found (Product in JSON format in response body)
    
    - 404: Price or product not found
    
        
**PUT: /api/v1/products/{id}**

- Updates the pricing value of the given product ID.

- Example valid JSON request body (name is not necessary):

   ```
   {
     "id": 50457530,
     "name": "Overwatch Origins Edition (PC Game)",
     "current_price": {
       "value": 19.99,
       "currency_code": "USD"
     }
   }
   ```
   
- Response codes:

    - 200: Price of given product ID successfully updated (Product with updated price returned in body)
    
    - 400: Provided price value is not valid
    
    - 404: Product ID not found
   
###Postman
As means of interacting with the running code, there is a Postman Collection file named "myRetail.postman_collection.json" is the root directory of the repo. This can be imported into the tool Postman.

###Product Price IDs Stored in MongoDB
At the start of the application, the following product price IDs are stored in the embedded MongoDB datastore. Performing GET requests with these product IDs will return valid JSON responses. Any other product ID will return a 404.

- 16696652

- 50457530

- 13860428

