package com.joshkleven.myRetail.functional

import com.joshkleven.myRetail.model.Price
import com.joshkleven.myRetail.model.Product
import com.joshkleven.myRetail.repository.PriceRepository
import org.hamcrest.core.StringContains
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.client.MockRestServiceServer
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import spock.lang.Unroll

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest
import static org.springframework.test.web.client.response.MockRestResponseCreators.withServerError
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess

/**
 * Created by joshkleven on 5/26/17.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Unroll
class ProductFunctionalSpec extends Specification{
    @Autowired
    TestRestTemplate testRestTemplate

    @Autowired
    PriceRepository priceRepository

    //RestTemplate restTemplate
    //MockRestServiceServer mockServer

    def setup(){
        priceRepository.deleteAll()
    }

    //TODO: mock out calls to redsky API instead of using real values for all test cases
    def 'Returns expected JSON data for product ID'(){
        setup:
        setup()
        Price price = new Price(id: 50457530, value: 59.99, currency_code: "USD")
        Product product = new Product(id:50457530, name: 'Overwatch Origins Edition (PC Game)', current_price: price)
        priceRepository.save(price)

        //mockServer = MockRestServiceServer.createServer(restTemplate)
        //mockServer.expect(requestTo(new StringContains("/v2/pdp/tcin/1234?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics")))
        //          .andExpect(method(HttpMethod.GET))
        //          .andRespond(withSuccess("{product: {item: { product_description: { title: 'What a Great Product'}}}}", MediaType.APPLICATION_JSON_UTF8))

        when:
        ResponseEntity<Product> responseGet = testRestTemplate.getForEntity("/api/v1/products/"+product.id.toString(), Product)

        then:
        responseGet.getStatusCodeValue() == 200
        responseGet.headers.getContentType() == MediaType.APPLICATION_JSON_UTF8

        Product getProduct = responseGet.body
        getProduct.id == product.id
        getProduct.name == product.name
        getProduct.current_price.value == product.current_price.value
        getProduct.current_price.currency_code == product.current_price.currency_code
    }

    def 'Returns error on #desc'(){
        setup:
        setup()
        Price price = new Price(id: 16483589, value: 59.99, currency_code: "USD")
        Product product = new Product(id: id, name: 'What a great name', current_price: price)
        priceRepository.save(price)

        when:
        ResponseEntity<Map> responseGet = testRestTemplate.getForEntity("/api/v1/products/"+product.id.toString(), Map)

        then:
        responseGet.getStatusCodeValue() == 404
        responseGet.headers.getContentType() == MediaType.APPLICATION_JSON_UTF8
        Map body = responseGet.body

        body.message == expectedMessage

        where:
        desc                    | id        || expectedMessage
        'invalid ID'            | 16483589  || 'Error reading product API'
        'unsaved Price for ID'  | 50730397  || 'Current Price for item 50730397 not found'
    }

    def 'PUT new product price'(){
        setup:
        setup()
        Price price = new Price(id: 50457530, value: 59.99, currency_code: "USD")
        Product product = new Product(id: 50457530, name: 'Overwatch Origins Edition (PC Game)', current_price: price)
        priceRepository.save(price)
        price.value = 39.99

        when:
        ResponseEntity<Product> responseEntity = testRestTemplate.exchange("/api/v1/products/"+product.id.toString(), HttpMethod.PUT, new HttpEntity<>(product), Product)

        then:
        responseEntity.getStatusCodeValue() == 200
        responseEntity.headers.getContentType() == MediaType.APPLICATION_JSON_UTF8
        Product returnedProduct = responseEntity.body
        returnedProduct.current_price.value == new Float(39.99)

        priceRepository.findOne(product.id).value == new Float(39.99)
    }

    def 'Attempt PUT on non-saved product ID'(){
        setup:
        setup()
        Price price = new Price(id: 50457530, value: 59.99, currency_code: "USD")
        Product product = new Product(id: 50457530, name: 'Overwatch Origins Edition (PC Game)', current_price: price)
        price.value = 39.99

        when:
        ResponseEntity<Map> responseEntity = testRestTemplate.exchange("/api/v1/products/"+product.id.toString(), HttpMethod.PUT, new HttpEntity<>(product), Map)

        then:
        responseEntity.getStatusCodeValue() == 404
        responseEntity.headers.getContentType() == MediaType.APPLICATION_JSON_UTF8
        Map body = responseEntity.body

        body.message == 'Price ID not found'
    }

    def 'Attempt PUT with null price'(){
        setup:
        setup()
        Price savedPrice = new Price(id: 50457530, value: 59.99, currency_code: "USD")
        priceRepository.save(savedPrice)

        Price nullPrice = new Price()
        Product product = new Product(id: 50457530, name: 'Overwatch Origins Edition (PC Game)', current_price: nullPrice)

        when:
        ResponseEntity<Map> responseEntity = testRestTemplate.exchange("/api/v1/products/"+product.id.toString(), HttpMethod.PUT, new HttpEntity<>(product), Map)

        then:
        responseEntity.getStatusCodeValue() == 400
        responseEntity.headers.getContentType() == MediaType.APPLICATION_JSON_UTF8
        Map body = responseEntity.body

        body.message == 'New price value not valid'
    }
}
