package com.joshkleven.myRetail.repository

import com.joshkleven.myRetail.model.Price
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.InvalidDataAccessApiUsageException
import spock.lang.Specification
import spock.lang.Unroll

import javax.validation.ConstraintViolationException

/**
 * Created by joshkleven on 5/26/17.
 */
@SpringBootTest
@Unroll
class PriceRepositorySpec extends Specification{
    @Autowired
    PriceRepository priceRepository

    def 'Price can be saved with valid values'(){
        setup:
        def startingPriceRecordsCount = priceRepository.count()
        Price price = new Price(id:123456789, value: 199.99, currency_code: 'USD')

        when:
        priceRepository.save(price)

        then:
        priceRepository.count() == startingPriceRecordsCount + 1

        when:
        Price priceCheck = priceRepository.findOne(price.id)

        then:
        priceCheck.id == new Long(123456789)
        priceCheck.value == new Float(199.99)
        priceCheck.currency_code == 'USD'
    }

    def 'Attempt save Price where #desc'(){
        setup:
        Price price = new Price(id: id, value: value, currency_code: currency_code)

        when:
        priceRepository.save(price)

        then:
        thrown(expectedException)

        where:
        desc                    | id            | value         | currency_code             || expectedException
        'id is null'            | null          | 19.99         | 'USD'                     || InvalidDataAccessApiUsageException
        'value is null'         | 1234          | null          | 'USD'                     || ConstraintViolationException
        'currency_code is null' | 1234          | 19.99         | null                      || ConstraintViolationException
    }

    def 'Saving to the same id twice will update the values'(){
        setup:
        Price price1 = new Price(id:123456789, value: 199.99, currency_code: 'USD')
        Price price2 = new Price(id:123456789, value: 77.77, currency_code: 'USD')

        when:
        priceRepository.save(price1)
        priceRepository.save(price2)
        Price priceCheck = priceRepository.findOne(123456789)

        then:
        priceCheck.value == new Float(77.77)

    }
}
