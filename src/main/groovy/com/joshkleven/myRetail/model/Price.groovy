package com.joshkleven.myRetail.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id

import javax.validation.constraints.NotNull

/**
 * Created by joshkleven on 5/23/17.
 */
class Price {
    @Id
    @JsonIgnore
    @NotNull
    Long id

    @NotNull
    // use BigDecimal type instead?
    Float value

    @NotNull
    String currency_code

}
