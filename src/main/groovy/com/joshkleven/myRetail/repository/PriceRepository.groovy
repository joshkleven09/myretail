package com.joshkleven.myRetail.repository

import com.joshkleven.myRetail.model.Price
import org.springframework.data.mongodb.repository.MongoRepository

/**
 * Created by joshkleven on 5/23/17.
 */
interface PriceRepository extends MongoRepository<Price, Long> {

}