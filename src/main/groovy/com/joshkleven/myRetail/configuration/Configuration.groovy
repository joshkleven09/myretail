package com.joshkleven.myRetail.configuration

import org.springframework.context.annotation.Bean

import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean

/**
 * Created by joshkleven on 5/26/17.
 */
@org.springframework.context.annotation.Configuration
class Configuration {
    @Bean
    ValidatingMongoEventListener validatingMongoEventListener() {
        return new ValidatingMongoEventListener(validator())
    }

    @Bean
    LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean()
    }
}
