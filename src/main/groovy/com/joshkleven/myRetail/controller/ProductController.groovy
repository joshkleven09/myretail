package com.joshkleven.myRetail.controller

import com.joshkleven.myRetail.model.Product
import com.joshkleven.myRetail.service.ProductService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletResponse

/**
 * Created by joshkleven on 5/23/17.
 */
@RestController
@RequestMapping("/api/v1/products")
class ProductController {
    @Autowired
    ProductService productService

    @GetMapping("/{id}")
    Product getProduct(@PathVariable Long id, HttpServletResponse response){
        return productService.buildProduct(id, response)
    }

    @PutMapping("/{id}")
    Product updateCurrentPrice(@PathVariable Long id, @RequestBody Product product, HttpServletResponse response){
        return productService.updateProductPrice(id, product, response)
    }
}
