package com.joshkleven.myRetail.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.joshkleven.myRetail.model.Price
import com.joshkleven.myRetail.model.Product
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

import javax.servlet.http.HttpServletResponse

/**
 * Created by joshkleven on 5/23/17.
 */
@Service
class ProductService {
    @Autowired
    PriceService priceService

    @Autowired
    ProductInfoService productInfoService

    Product buildProduct(Long id, HttpServletResponse response){
        Product product = new Product(id: id)

        ResponseEntity res = productInfoService.getProductInfo(id)

        if(res.getStatusCodeValue() == 200){
            ObjectMapper mapper = new ObjectMapper()

            Object obj = mapper.readValue(res.getBody(), Object)
            product.name = obj.product.item.product_description.title

            Price price = priceService.getPriceById(id)

            if(!price){
                response.sendError(404, "Current Price for item " + id + " not found")
            }
            else {
                product.current_price = price
            }
        }
        else{
            response.sendError(res.getStatusCodeValue(), "Error reading product API")
        }

        return product
    }

    Product updateProductPrice(Long id, Product product, HttpServletResponse response){
        Product returnProduct = new Product()

        if(product.current_price.value != null){
            priceService.updatePrice(id, product.current_price.value, response)

            try{
                returnProduct = buildProduct(id, response)
            } catch (IllegalStateException e) {
                //error already sent via HttpServletResponse
            }
        }
        else{
            response.sendError(400, "New price value not valid")
        }


        return returnProduct
    }
}
