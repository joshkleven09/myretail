package com.joshkleven.myRetail.service

import com.joshkleven.myRetail.model.Price
import com.joshkleven.myRetail.repository.PriceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.servlet.http.HttpServletResponse

/**
 * Created by joshkleven on 5/23/17.
 */
@Service
class PriceService {
    @Autowired
    PriceRepository priceRepository

    Price getPriceById(Long id){
        return priceRepository.findOne(id)
    }

    Price updatePrice(Long id, Float newValue, HttpServletResponse response){
        Price price = priceRepository.findOne(id)

        if(!price){
            response.sendError(404, "Price ID not found")
        }
        else {
            price.value = newValue

            priceRepository.save(price)
        }

        return price
    }
}
