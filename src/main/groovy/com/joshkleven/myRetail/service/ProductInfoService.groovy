package com.joshkleven.myRetail.service

import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate

/**
 * Created by joshkleven on 5/23/17.
 */
@Service
class ProductInfoService {
    String baseURL = "http://redsky.target.com/v2/pdp/tcin/"
    String urlParams = "?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics"

    ResponseEntity<String> getProductInfo(Long id){
        RestTemplate restTemplate = new RestTemplate()

        try{
            restTemplate.getForEntity(baseURL + id.toString() + urlParams, String)
        } catch (HttpClientErrorException e){
            return new ResponseEntity([error: e.message], e.statusCode)
        }
    }
}
