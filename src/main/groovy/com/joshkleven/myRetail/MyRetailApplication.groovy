package com.joshkleven.myRetail

import com.joshkleven.myRetail.model.Price
import com.joshkleven.myRetail.repository.PriceRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class MyRetailApplication {

	static void main(String[] args) {
		SpringApplication.run MyRetailApplication, args
	}

	@Bean
	CommandLineRunner onStart(PriceRepository priceRepository) {
		return new CommandLineRunner() {
			@Override
			void run(String... args) throws Exception {

				priceRepository.deleteAll()

				// save some pricing values to the embedded MongoDB
				priceRepository.save(new Price(id: 16696652, value: 278.99, currency_code: "USD"))
				priceRepository.save(new Price(id: 50457530, value: 59.99, currency_code: "USD"))
				priceRepository.save(new Price(id: 13860428, value: 19.98, currency_code: "USD"))
			}
		}
	}
}
